# Portfolio 2019

Uses Webpack 4, React, Babel, SCSS.

Setup (Once repo has been cloned from Bitbucket)
To install dependencies, in project root: 'npm install'

Development:
To start Webpack Dev Server: 'npm run dev'
You should then be able to see the site at 'localhost:8080'
Development files including manifest and service worker can be found within the 'portfolio' folder

Production:
To package for production: 'npm run prod'
This will copy, compress & package all files into the 'dist' folder
