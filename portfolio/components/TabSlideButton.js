import PropTypes from 'prop-types';
import '../assets/scss/tabSlideBtn.scss';

const React = require('react');

// Component will return buttons for either a tabs or slider
// setup (these are identical on desktop but different on mobile - CSS changes
// are applied using a class.)
function TabSlideButton(props) {

    return (
        <button
            type="button"
            className={props.type === 'tab' ? 'tab-slide-btn tab-btn' : 'tab-slide-btn slider-btn'}
            aria-label={props.title}
            aria-haspopup="true"
            aria-expanded={props.buttonIndex === props.currentButton}
            aria-controls={`slide-tab-${props.id}`}
            onClick={() => props.onClick(props.buttonIndex)}
            style={{ color: props.colour === '#F4E806' ? '#222' : '#FFF' }}
        >
            <div className="button-slider" style={{ backgroundColor: props.colour }} />
            <div className="button-slider-mob" style={{ backgroundColor: props.colour }} />
            <span>{props.title}</span>
        </button>
    );
}

TabSlideButton.propTypes = {
    type: PropTypes.string.isRequired,
    colour: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    buttonIndex: PropTypes.number.isRequired,
    currentButton: PropTypes.number.isRequired,
};

export default TabSlideButton;
