import { Route } from 'react-router-dom';
import Loadable from 'react-loadable';
import LoadingComponent from './LoadingComponent';
import '../assets/scss/portfolioPage.scss';

const React = require('react');

// Code splitting using Loadable
// Routes are set here to indivudual pages
const SkillsTabs = Loadable({
    // import is not an error here as we have babel plugin 'syntax-dynamic-import'
    loader: () => import('./SkillsTabs'),
    loading: LoadingComponent,
    timeout: 10000,
});

const ExperienceSlider = Loadable({
    loader: () => import('./ExperienceSlider'),
    loading: LoadingComponent,
    timeout: 10000,
});

const About = Loadable({
    loader: () => import('./About'),
    loading: LoadingComponent,
    timeout: 10000,
});

class PortfolioPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            mounted: false,
        };
    }

    componentDidMount() {
        window.scrollTo(0, 0);

        setTimeout(() => {
            this.setState({ mounted: true });
        }, 0);
    }

    render() {
        return (
            <div className={`portfolio-page ${this.state.mounted ? 'fade-in' : ''}`}>
                <div className="page-shadow" />
                <div className="portfolio-content">
                    <Route path="/skills" component={SkillsTabs} />
                    <Route path="/experience-and-qualifications" component={ExperienceSlider} />
                    <Route path="/about" component={About} />
                </div>
            </div>
        );
    }
}

export default PortfolioPage;
