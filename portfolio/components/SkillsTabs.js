import TabSlideButton from './TabSlideButton';
import SkillsCard from './SkillsCard';
import '../assets/scss/skillsTabs.scss';

const React = require('react');

// Tabs
const tabs = [
    {
        title: 'Development Skills',
        id: 'dev',
        colour: '#427AA9',
    },
    {
        title: 'Supporting Skills',
        id: 'supporting',
        colour: '#F4E806',
    },
];

// Cards for the 'dev' tab
const devCards = [
    {
        tagList: {
            lang: ['Vanilla JS', 'CSS/SASS/LESS', 'HTML5', 'PHP'],
            skill: ['Animation'],
            lib: ['React', 'Jquery', 'D3', 'Greensock', 'Wordpress'],
        },
        description: 'I am highly experienced with Javascript (vanilla JS and various libraries), HTML5 and CSS/SASS/LESS. I also have some experience with PHP, and with templating for various frameworks. I enjoy working as part of a team, and as code is often shared, am used to writing in a clear and well commented style, and adhering to a style guide.',
    },
    {
        tagList: { tool: ['Gulp', 'Grunt', 'NPM', 'Webpack', 'Git'] },
        description: "I have used all of the above build tools, and currently use a Webpack 4 or Gulp/Babel/Browserify setup. I'm also very used to using Git for versioning, branching and managing pull requests.",
    },
    {
        tagList: {
            skill: ['Performance', 'Optimisation'],
            lang: ['PWA'],
        },
        description: 'For me, optimization is a key part of the development process, both while building (using faster and more efficient animations & techniques) and when packaging (compressing & concatenating files and images, considering caching etc.) Everything I design and build is responsive, and in some cases will be designed mobile first, or will be a PWA, depending on the manner it will be used.',
    },
    {
        tagList: {
            skill: ['Compatibility', 'Testing'],
            lib: ['Karma', 'Jasmine', 'Cypress'],
        },
        description: 'Part of the build process is always testing across all relevant browsers and devices or mail clients, and where necessary building in fallbacks which deliver the required content clearly and without issues. I use automated testing for some site components, and have used Karma, Jasmine and Cypress.',
    },
    {
        tagList: { skill: ['Accessibility'] },
        description: 'Accessibility is hugely important, and so I try to make sure that everything I build follows all current best practice. I use Aria labels and descriptions, design where focus should move for smooth interactions and test with Chrome vox to ensure that content can be as easily reached with a keyboard as a mouse, and is also accessible to a non or partially sighted user.',
    },
];

// cards for the 'supporting' tab
const otherCards = [
    {
        tagList: { skill: ['UX', 'UI'] },
        description: 'One of my previous roles - at the Philharmonia Orchestra - involved a large amount of UI and UX design, particularly in redesigning the purchase pathway and other areas of the site for enhanced usability and responsiveness. I used personas, user journeys, wireframes/prototypes and site-mapping to identify where interactions could be made easier and to remove points of frustration.',
    },
    {
        tagList: {
            skill: ['Web & Print Design'],
            tool: ['Sketch', 'Abstract', 'Illustrator', 'Photoshop'],
        },
        description: 'My work at the Philharmonia also involved a lot of visual design, both for web and print. There are different considerations for each, but needing to do both has made me both a better designer and a better developer, as the goal is always to create something that is pleasing to interact with and easy to use, and the wider a range of products I work on, the more varied problems and solutions I discover.',
    },
    {
        tagList: { skill: ['Time Management', 'Organistaion'] },
        description: 'In every place I have worked projects have sometimes had extremely tight deadlines. I have experience juggling multiple projects and prioritising to make sure they are all delivered on time, and this has taught me to keep calm under pressure.',
    },
    {
        tagList: { skill: ['Agile Methodologies', 'Remote Working'] },
        description: 'I have experience with using various agile methods to manage work and provide clear information to stakeholders about the status of a ticket or sprint, and to track progress, priorities and time spent on an issue. I have also worked remotely, and am happy to work independantly.',
    },
];


class SkillsTabs extends React.Component {

    static makeSkillCards(cardsArray) {
        const cardsToRender = [];

        for (let index = 0; index < cardsArray.length; index += 1) {
            cardsToRender.push(
                <SkillsCard
                    tagList={cardsArray[index].tagList}
                    description={cardsArray[index].description}
                    key={index}
                />,
            );
        }

        return (
            <div>{cardsToRender}</div>
        );
    }

    constructor(props) {
        super(props);

        this.state = {
            currentTab: 0,
        };
    }

    tabButtonClick(tabIndex) {
        this.setState({
            currentTab: tabIndex,
        });
    }

    render() {
        return (
            <div className="content-holder">
                <div className="tabs intro-area">
                    <h2>Skills</h2>
                    <div className="tabs-holder">
                        {tabs.map((tab, index) => (
                            <TabSlideButton
                                type="tab"
                                colour={tab.colour}
                                title={tab.title}
                                id={tab.id}
                                onClick={() => this.tabButtonClick(index)}
                                buttonIndex={index}
                                currentButton={this.state.currentTab}
                                key={`tab-btn-${index}`}
                            />
                        ))}
                    </div>
                    <div className="desktop-only">
                        <div className="key">
                            <span className="keypoint lang" />
                            <span>Languages & Standards</span>
                        </div>
                        <div className="key">
                            <span className="keypoint lib" />
                            <span>Libraries & Frameworks</span>
                        </div>
                        <div className="key">
                            <span className="keypoint tool" />
                            <span>Tools & Software</span>
                        </div>
                        <div className="key">
                            <span className="keypoint skill" />
                            <span>Skills & Experience</span>
                        </div>
                    </div>
                </div>
                <div className="content-area">
                    <div id="dev-tab" className={this.state.currentTab === 0 ? 'tab-content active' : 'tab-content'}>
                        {SkillsTabs.makeSkillCards(devCards)}
                    </div>
                    <div id="other-tab" className={this.state.currentTab === 1 ? 'tab-content active' : 'tab-content'}>
                        {SkillsTabs.makeSkillCards(otherCards)}
                    </div>
                </div>
            </div>
        );
    }
}

export default SkillsTabs;
