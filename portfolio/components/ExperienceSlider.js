import Swipeable from 'react-swipeable';
import TabSlideButton from './TabSlideButton';
import ExperienceSlide from './ExperienceSlide';
import '../assets/scss/expSlider.scss';

const React = require('react');

// Description is broken up into an array per span - to add a link, the href should
// be included as the second item in the span array. This allows for links to be included
// in the slide component, and for tabIndex to update with the slider movement (so that
// links may not be tabbed to when they should be hidden.)
const slides = [
    {
        company: 'The Developer Society',
        id: 'dev',
        colour: '#F4E806',
        dates: 'September 2018 - December 2018',
        position: 'Front-End Developer',
        description: [
            ['I worked at the Developer Society for a relatively short time, but while there I was fortunate to work on a wide range of great projects - in some cases making additions or fixing bugs on existing sites, and in some cases building up something new from scratch, according to sprint priorities. Dev had a setup which encouraged remote working, and I found that that worked very well for me.'],
        ],
    },
    {
        company: 'MVF Global',
        id: 'mvf',
        colour: '#D63F24',
        dates: 'November 2016 - September 2018',
        position: 'Front-End Engineer',
        description: [
            ['At MVF we used an agile approach, and so in my time there I completed many small tasks across a large number of sites as they were prioritised for attention. However in my last 6 months there I created the front end for a full rebuild of a large existing site - '],
            ['Movehub', 'https://www.movehub.com/uk'],
            [". I also helped to set up and organise a 'Front-End Guild', where we met to discuss best practice, learn, share discoveries and agree standards. One of my main objectives at MVF was to raise the standard of accessibility across all associated sites, and I gave a tech talk and wrote best practice guides & checklists to help with this."],
        ],
    },
    {
        company: 'The Philharmonia',
        id: 'philharmonia',
        colour: '#1D2654',
        dates: 'October 2015 - October 2016',
        position: 'Front-End Developer and Designer',
        description: [
            ['As the Front-End Developer and Designer for the Philharmonia Orchestra, I worked across a variety of projects, from website troubleshooting and iterative enhancement, to the design and build of mini sites and the print design for season brochures. '],
            ['The Stravinsky microsite', 'http://www.philharmonia.co.uk/stravinsky/'],
            [' was built to accompany a series of concerts focussed on composer Igor Stravinsky. I was responsible for the full design, build and testing, working from a partially existing visual identity. This site won bronze in the 2017 Lovie web awards [category - websites - music].'],
        ],
    },
    {
        company: 'Mohawk (formerly Muirhoward)',
        id: 'mh',
        colour: '#DC3232',
        dates: 'June 2012 - October 2015',
        position: 'Interactive Developer',
        description: [
            ['In my time at this agency I worked on apps, sites, emails, animated banners and event screens. I began working with Flash and Actionscript 3, but moved into HTML5 and Javascript based projects as industry demand changed. I progressed from junior to full Interactive Developer in 2013, and worked on a variety of different projects for many large and well known clients.'],
        ],
    },
    {
        company: 'Yoowalk',
        id: 'yoowalk',
        colour: '#427AA9',
        dates: 'June 2009 - May 2010',
        position: 'Graphiste / Graphic Designer (trainee)',
        description: [
            ['I worked at Yoowalk (a startup which created a 3D virtual world for users to navigate around and be immersed in) between the 2nd and 3rd year of my degree. Coventry University took part in the Leonardo da Vinci scheme, which allowed a few students to win a grant to undertake work experience in another European country. My work there was mostly graphics and artwork based (asset creation, interface design), but I was also encouraged to begin learning and working with HTML and CSS, and realised the possibilities of development.'],
        ],
    },
];

class ExperienceSlider extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            currentSlide: 0,
            totalSlides: slides.length,
        };
    }

    // React swipeable used for touch events
    swipeLeft() {
        if (this.state.currentSlide < this.state.totalSlides - 1) {
            const newSlideIndex = this.state.currentSlide + 1;
            this.jumpToSlide(newSlideIndex);
        }
    }

    swipeRight() {
        if (this.state.currentSlide > 0) {
            const newSlideIndex = this.state.currentSlide - 1;
            this.jumpToSlide(newSlideIndex);
        }
    }

    jumpToSlide(slideIndex) {
        this.setState({
            currentSlide: slideIndex,
        });
    }

    render() {
        return (
            <div>
                <div className="content-holder">
                    <div className="intro-area">
                        <h2>Qualifications &amp; Experience</h2>
                        <p>(References available on request)</p>
                    </div>
                    <div className="content-area shadow-holder">
                        <div id="qualifications">
                            <div className="qualification-card">
                                <p>
                                    <span className="h3">BA Hons First Class in Illustration &amp; Animation </span>
                                    <span className="desktop-only" />
                                    Coventry University, September 2006 - June 2011
                                </p>
                                <p>
                                    <span className="bold">3 A2 Levels: History </span>
                                    [A],
                                    <span className="bold"> Graphic Design </span>
                                    [A],
                                    <span className="bold"> Art </span>
                                    [A],
                                    <span className="desktop-only" />
                                    <span className="bold"> 2 AS Levels - French </span>
                                    [A],
                                    <span className="bold"> Maths </span>
                                    [A]
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="content-holder">
                    <div className="intro-area slider-btns-holder">
                        {slides.map((slide, index) => (
                            <TabSlideButton
                                type="slide"
                                colour={slide.colour}
                                title={slide.company}
                                id={slide.id}
                                onClick={() => this.jumpToSlide(index)}
                                buttonIndex={index}
                                currentButton={this.state.currentSlide}
                                key={`slide-btn-${slide.id}`}
                            />
                        ))}
                    </div>
                    <div className="content-area shadow-holder">
                        <div
                            className="slides-holder"
                            style={{
                                width: `${100 * slides.length}%`,
                                transform: `translateX(${(100 / slides.length) * this.state.currentSlide * -1}%)`,
                            }}
                        >
                            {slides.map((slide, index) => (
                                <ExperienceSlide
                                    colour={slide.colour}
                                    company={slide.company}
                                    dates={slide.dates}
                                    position={slide.position}
                                    description={slide.description}
                                    id={slide.id}
                                    slideIndex={index}
                                    currentSlide={this.state.currentSlide}
                                    totalSlides={slides.length}
                                    key={`slide-${slide.id}`}
                                />
                            ))}
                            <Swipeable
                                className="swiper"
                                trackMouse
                                onSwipedLeft={() => this.swipeLeft()}
                                onSwipedRight={() => this.swipeRight()}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ExperienceSlider;
