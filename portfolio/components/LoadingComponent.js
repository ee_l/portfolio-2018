import PropTypes from 'prop-types';
import '../assets/scss/loadingComponent.scss';

const React = require('react');

// Loading component - may show on slower connections when loading the detail view
function LoadingComponent(props) {

    let message = 'Loading...';

    if (props.error) {
        message = 'Error - please refresh the page to try again';

    } else if (props.timedOut) {
        message = 'Sorry, this is taking a while...';

    }

    if (props.pastDelay) {
        return (
            <div className="loader">
                <p>{message}</p>
                <div className="loader-ico">
                    <svg width="54px" height="54px" viewBox="0 0 54 54">
                        <path
                            fill="none"
                            stroke="#000000"
                            strokeWidth="2"
                            d="M51.973,27.014c0,13.674-11.086,24.76-24.761,24.76s-24.76-11.086-24.76-24.76c0-13.675,11.085-24.761,24.76-24.761c4.768,0,9.221,1.347,12.999,3.682"
                        />
                    </svg>
                </div>
            </div>
        );
    }

    return null;
}

LoadingComponent.propTypes = {
    error: PropTypes.bool,
    timedOut: PropTypes.bool,
    pastDelay: PropTypes.bool,
};

LoadingComponent.defaultProps = {
    error: false,
    timedOut: false,
    pastDelay: false,
};

export default LoadingComponent;
