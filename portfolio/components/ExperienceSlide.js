import PropTypes from 'prop-types';
import DescriptionSection from './DescriptionSection';
import '../assets/scss/expSlides.scss';

const React = require('react');

// Slide component for experience slider
function ExperienceSlide(props) {

    return (
        <div
            id={`slide-tab-${props.id}`}
            className={props.slideIndex === props.currentSlide
                ? 'slide-content active' : 'slide-content'}
            style={{ width: `${100 / props.totalSlides}%` }}
            aria-hidden={props.slideIndex !== props.currentSlide}
        >
            <div className="exp-card">
                <div className="colour-strip" style={{ backgroundColor: props.colour }} />
                <div className="exp-header">
                    <p>{props.dates}</p>
                    <h3>{props.company}</h3>
                    <p>{props.position}</p>
                </div>
                <DescriptionSection
                    components={props.description}
                    allowTabToLinks={props.slideIndex === props.currentSlide}
                />
                <div className="colour-strip mobile-only" style={{ backgroundColor: props.colour }} />
            </div>
        </div>
    );
}

ExperienceSlide.propTypes = {
    colour: PropTypes.string.isRequired,
    company: PropTypes.string.isRequired,
    dates: PropTypes.string.isRequired,
    position: PropTypes.string.isRequired,
    description: PropTypes.arrayOf(
        PropTypes.arrayOf(
            PropTypes.string,
        ),
    ).isRequired,
    id: PropTypes.string.isRequired,
    slideIndex: PropTypes.number.isRequired,
    currentSlide: PropTypes.number.isRequired,
    totalSlides: PropTypes.number.isRequired,
};

export default ExperienceSlide;
