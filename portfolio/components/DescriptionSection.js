import PropTypes from 'prop-types';
import '../assets/scss/descriptionSection.scss';

const React = require('react');

// DescriptionSection allows for a section of text including spans and links.
// The allowTabToLinks property ensures that when this component is used in a slider,
// links can only be tabbed to when they are visible.
class DescriptionSection extends React.Component {

    makeDescriptionSection(descriptionArray) {
        const descriptionComponents = [];

        for (let index = 0; index < descriptionArray.length; index += 1) {
            if (descriptionArray[index].length === 1) {
                // Add span of text
                descriptionComponents.push(<span key={`span-${index}`}>{descriptionArray[index][0]}</span>);
            } else if (descriptionArray[index].length === 2) {
                // Add link
                descriptionComponents.push(
                    <a
                        className="alternative-focus inline-link"
                        href={descriptionArray[index][1]}
                        target="_blank"
                        rel="noopener noreferrer"
                        tabIndex={this.props.allowTabToLinks ? 0 : -1}
                        key={`link-${index}`}
                    >
                        {descriptionArray[index][0]}
                    </a>,
                );
            }
        }

        return (descriptionComponents);
    }

    render() {
        return (
            <span>
                {this.makeDescriptionSection(this.props.components)}
            </span>
        );
    }
}

DescriptionSection.propTypes = {
    components: PropTypes.arrayOf(
        PropTypes.arrayOf(
            PropTypes.string,
        ),
    ).isRequired,
    allowTabToLinks: PropTypes.bool.isRequired,
};

export default DescriptionSection;
