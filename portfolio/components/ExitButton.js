import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import '../assets/scss/exitButton.scss';

const React = require('react');

// Exit link - return to home page from detail view
function ExitButton(props) {

    return (
        <div className={`exit-button ${props.notHomePage ? '' : 'hidden'}`}>
            <div aria-hidden="true">
                <svg width="50px" height="50px" viewBox="0 0 55 55">
                    <polygon points="33.584,20.101 27.501,26.184 21.417,20.101 20.101,21.417 26.185,27.5 20.101,33.582 21.417,34.898 27.501,28.814 33.586,34.898 34.9,33.582 28.816,27.5 34.9,21.416" />
                </svg>
            </div>
            <Link
                to="/"
                className="alternative-focus"
                aria-label="Exit detail view"
                aria-hidden={!props.notHomePage}
                tabIndex={props.notHomePage ? '0' : '-1'}
            />
        </div>
    );
}

ExitButton.propTypes = {
    notHomePage: PropTypes.bool.isRequired,
};

export default ExitButton;
