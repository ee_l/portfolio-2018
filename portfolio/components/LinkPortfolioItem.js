import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { AppContext } from './AppContext';
import DescriptionSection from './DescriptionSection';
import '../assets/scss/linkPortfolioItem.scss';

const React = require('react');

class LinkPortfolioItem extends React.Component {

    componentDidMount() {
        // Applies only to internal links
        if (this.props.internalLinkTo) {
            const classes = this.itemLink.props.className;
            // Experiment with context api, ref and innerref to allow focus of
            // last selected item with use of routes
            if (classes && classes.indexOf('has-prefocus') !== -1) {
                this.itemBtn.focus();
            }
        }
    }

    render() {
        const foregroundClass = this.props.foregroundColour === '#fff' ? 'dark-content' : 'light-content',
            backgroundClass = this.props.backgroundColour === '#e6e7e8' ? 'light-bg' : 'dark-bg',
            linkClasses = `${foregroundClass} ${backgroundClass} alternative-focus`;

        let linkComponent = null,
            contentComponent = null;

        // The portfolioItem will be made up of 2 parts which may be combined in different ways
        // - the 'linkComponent' (internal, external, or none) and the 'contentComponent'
        // (text, image, or both)

        // Determine item content
        if (this.props.image.length > 0) {
            // Both text and image, but no external link item - position text at top left
            if ((this.props.textHead.length > 0 || this.props.textDescription.length > 0)
            && !this.props.externalLinkTo) {
                contentComponent = (
                    <div>
                        <div className="portfolio-item-text">
                            <span className="text-head">{this.props.textHead}</span>
                            <span>{this.props.textDescription}</span>
                        </div>
                        <div className="item-preview" style={{ backgroundColor: this.props.foregroundColour }}>
                            <img src={this.props.image} alt="" />
                        </div>
                    </div>
                );
            } else {
                // Either text or image, or both with external link icon - position both
                // in sliding item-preview area (because external links have an icon in top-left)
                contentComponent = (
                    <div className="item-preview" style={{ backgroundColor: this.props.foregroundColour }}>
                        <img src={this.props.image} alt="" />
                        <div className="portfolio-item-text">
                            <span className="text-head">{this.props.textHead}</span>
                            <span>{this.props.textDescription}</span>
                        </div>
                    </div>
                );
            }
        } else {
            // Text only (foreground colour provides contrast)
            contentComponent = (
                <div className="item-preview" style={{ backgroundColor: this.props.foregroundColour }}>
                    <div className="portfolio-item-text">
                        <span className="text-head">{this.props.textHead}</span>
                        <span>{this.props.textDescription}</span>
                    </div>
                </div>
            );
        }

        // Determine link type
        // Internal link - 'Link' component
        if (this.props.internalLinkTo.length > 0) {
            linkComponent = (
                <AppContext.Consumer>
                    {context => (
                        <li className="item">
                            <Link
                                to={`/${this.props.internalLinkTo}`}
                                className={`alternative-focus item-link ${(this.props.itemIndex === context.previousItem) ? 'has-prefocus' : ''}`}
                                ref={(LinkEl) => { this.itemLink = LinkEl; }}
                                innerRef={(a) => { this.itemBtn = a; }}
                                onClick={() => context.itemButtonClick(this.props.itemIndex)}
                                aria-label={this.props.textHead}
                            />
                            <div
                                className={`${linkClasses}`}
                                style={{ backgroundColor: this.props.backgroundColour }}
                                aria-hidden="true"
                            >
                                {contentComponent}
                            </div>
                        </li>
                    )}
                </AppContext.Consumer>
            );
        } else if (this.props.externalLinkTo.length > 0) {
            // External link - 'a' tag & external link icon
            linkComponent = (
                <li className="item">
                    <a
                        href={this.props.externalLinkTo}
                        target="_blank"
                        rel="noopener noreferrer"
                        className="item-link alternative-focus"
                        aria-label={this.props.textHead}
                    >
                        <span className="screenreader-only">{this.props.textHead}</span>
                    </a>
                    <div
                        className={`${linkClasses}`}
                        style={{ backgroundColor: this.props.backgroundColour }}
                        aria-hidden="true"
                    >
                        <svg width="55px" height="55px" viewBox="0 0 55 55">
                            <g>
                                <path d="M27.624,1.517c-14.235,0-25.816,11.581-25.816,25.816c0,14.235,11.581,25.815,25.816,25.815s25.815-11.58,25.815-25.815C53.439,13.098,41.859,1.517,27.624,1.517z M27.624,51.289c-13.209,0-23.956-10.746-23.956-23.956c0-13.209,10.746-23.955,23.956-23.955c13.209,0,23.955,10.746,23.955,23.955C51.579,40.543,40.833,51.289,27.624,51.289z" />
                                <polygon points="32.276,20.793 35.998,24.516 18.073,24.516 18.073,33.728 19.934,33.728 19.934,26.376 35.999,26.376 32.275,30.101 33.591,31.416 39.55,25.437 33.592,19.478" />
                            </g>
                        </svg>
                        {contentComponent}
                    </div>
                </li>
            );
        } else {
            // Non-link blocks may also exist. These would be expected to have a Header
            // and description.
            linkComponent = (
                <li className="item non-link-item">
                    <div className="non-link-preview item-preview" style={{ backgroundColor: this.props.backgroundColour }}>
                        <div className="non-link-text">
                            <span className="text-head">{this.props.textHead}</span>
                            <DescriptionSection
                                components={this.props.textDescription}
                                allowTabToLinks
                            />
                        </div>
                    </div>
                </li>
            );
        }

        return linkComponent;
    }
}

LinkPortfolioItem.propTypes = {
    backgroundColour: PropTypes.string.isRequired,
    foregroundColour: PropTypes.string,
    internalLinkTo: PropTypes.string,
    externalLinkTo: PropTypes.string,
    textHead: PropTypes.string.isRequired,
    textDescription: PropTypes.arrayOf(
        PropTypes.arrayOf(
            PropTypes.string,
        ),
    ).isRequired,
    image: PropTypes.string,
    itemIndex: PropTypes.number.isRequired,
};

LinkPortfolioItem.defaultProps = {
    foregroundColour: '#fff',
    internalLinkTo: '',
    externalLinkTo: '',
    image: '',
};

export default LinkPortfolioItem;
