import PropTypes from 'prop-types';
import '../assets/scss/header.scss';

const React = require('react');

function Header(props) {
    return (
        <header>
            <h1 className="header-info">
                <span>{props.name}</span>
                <span>{props.year}</span>
            </h1>
            <svg
                width="112.368px"
                height="71.959px"
                viewBox="0 0 112.368 71.959"
                enableBackground="new 0 0 112.368 71.959"
                xmlSpace="preserve"
                aria-hidden="true"
            >
                <g fill="#DC3232">
                    <polygon points="14.001,0 14.001,14.392 28.393,0" />
                    <polygon points="28.393,43.176 28.393,57.567 42.784,43.176" />
                    <polygon points="97.977,57.567 97.977,71.959 112.368,57.567" />
                    <polygon points="70.09,43.176 84.481,28.783 70.09,28.783" />
                    <polygon points="55.698,28.379 70.09,14.184 48.698,14.184 55.698,21.184" />
                    <polygon points="42.784,71.959 57.176,57.567 42.784,57.567" />
                    <polygon points="97.874,57.566 97.874,43.176 83.481,43.176" />
                    <polygon points="71.568,71.959 71.568,57.567 57.176,57.567" />
                    <polygon points="28.393,71.959 28.393,57.568 14,57.568" />
                    <polygon points="28.394,14.392 14.001,14.392 28.394,28.783" />
                    <polygon points="7,21.184 7,14.184 0,14.184" />
                    <polygon points="69.895,43.176 62.895,43.176 69.895,50.176" />
                    <polygon points="105.172,43.176 105.172,50.176 112.172,43.176" />
                </g>
            </svg>
        </header>
    );
}

Header.propTypes = {
    name: PropTypes.string.isRequired,
    year: PropTypes.string.isRequired,
};

export default Header;
