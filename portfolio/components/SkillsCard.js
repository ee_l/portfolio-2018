import PropTypes from 'prop-types';
import '../assets/scss/skillsCard.scss';

const React = require('react');

// Skills 'card' with tag list
class SkillsCard extends React.Component {

    static makeTaglist(taglistObject) {
        const tagsToRender = [],
            tagTypes = Object.keys(taglistObject);

        for (let index = 0; index < tagTypes.length; index += 1) {
            const tagType = tagTypes[index],
                tagsByType = taglistObject[tagType];

            for (let innerIndex = 0; innerIndex < tagsByType.length; innerIndex += 1) {
                tagsToRender.push(
                    <span className={tagType} key={`tag-${index}-${innerIndex}`}>
                        {tagsByType[innerIndex]}
                    </span>,
                );
            }
        }

        return (
            <div className="tag-list">
                {tagsToRender}
            </div>
        );
    }

    render() {
        return (
            <div className="skills-card">
                {SkillsCard.makeTaglist(this.props.tagList)}
                <p>{this.props.description}</p>
            </div>
        );
    }
}

SkillsCard.propTypes = {
    tagList: PropTypes.objectOf(
        PropTypes.arrayOf(
            PropTypes.string,
        ),
    ).isRequired,
    description: PropTypes.string.isRequired,
};

export default SkillsCard;
