const React = require('react');

const AppContext = React.createContext();

// AppContext allows for nested child components to access props without them
// being passed down through each parent
class AppProvider extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // Keep track of last selected item to return focus to that point on
            // return to home page view
            previousItem: -1,
            itemButtonClick: (buttonIndex) => {
                this.setState({ previousItem: buttonIndex });
            },
        };
    }

    render() {
        return (
            <AppContext.Provider value={{
                previousItem: this.state.previousItem,
                itemButtonClick: this.state.itemButtonClick,
            }}
            >
                {this.props.children}
            </AppContext.Provider>
        );
    }
}

export { AppProvider, AppContext };
