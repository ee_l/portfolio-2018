import PropTypes from 'prop-types';
import '../assets/scss/detailImage.scss';

const React = require('react');

// Detail Image (shown in detail view - e.g. about page)
class DetailImage extends React.Component {

    constructor(props) {
        super(props);

        this.handleImageLoaded = this.handleImageLoaded.bind(this);
        this.state = {
            isLoaded: false,
        };
    }

    // Animate in once loaded
    handleImageLoaded() {
        this.setState({
            isLoaded: true,
        });
    }

    render() {
        const imgClasses = this.state.isLoaded ? 'detail-img loaded-img' : 'detail-img';

        return (
            <div className={imgClasses}>
                <img
                    alt={this.props.alt}
                    src={this.props.src}
                    onLoad={this.handleImageLoaded}
                />
                <p>{this.props.description}</p>
            </div>
        );
    }
}

DetailImage.propTypes = {
    src: PropTypes.string.isRequired,
    alt: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
};

export default DetailImage;
