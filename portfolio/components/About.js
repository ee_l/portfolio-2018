import DetailImage from './DetailImage';
import illustration1 from '../img/some_turned_into_birds.jpg';
import illustration2 from '../img/shrimp_as_big_as_whales.jpg';
import label1 from '../img/albion-1.png';

const React = require('react');

// About page

const images = [
    {
        src: illustration1,
        alt: 'Book Illustration: leaves fell and became birds',
        description: 'I studied Illustration and Animation, and this is still something that I really enjoy doing in my spare time.',
    },
    {
        src: illustration2,
        alt: 'Book Illustration: shrimp the size of whales, and whales the size of shrimp',
        description: 'This and the above image are book Illustrations I made for "The First Something" by Theo Reyna.',
    },
    {
        src: label1,
        alt: 'Packaging label: Principia',
        description: 'This is one of a series of beer labels I designed as part of a small but fun project for Bath based micro-brewery Albion Brewing Company.',
    },
];

const sideText = [
    "I'm a Front-End specialist with over 6 years of experience, and am particularly interested in the combination of technically excellent development and animation with an awareness of design and UX, and in the conjunction of these things, making sites and apps that are truly pleasing to use and interact with.",
    'I am self-motivated and hard working, and very committed to any project that I am involved in. I have a passion for learning, and am always looking at new technology and building upon past experience to improve my methods.',
    "I'm a keen runner and also love walking, illustration and drinking lots of tea.",
    'Contact: el_ten@outlook.com',
];

function About() {

    return (
        <div className="content-holder">
            <div className="intro-area">
                <h2>About</h2>
                <div className="side-text">
                    {sideText.map((text, index) => <p key={index}>{text}</p>)}
                </div>
            </div>
            <div className="content-area">
                {images.map(image => (
                    <DetailImage
                        src={image.src}
                        alt={image.alt}
                        description={image.description}
                        key={image.src}
                    />
                ))}
            </div>
        </div>
    );
}

export default About;
