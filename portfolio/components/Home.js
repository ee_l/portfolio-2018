import LinkPortfolioItem from './LinkPortfolioItem';
import introThumbnail from '../img/intro-thumb.png';
import illustrationThumbnail from '../img/illustration-thumb.jpg';
import identityThumbnail from '../img/identity-thumb.jpg';
import stravinskyThumbnail from '../img/stravinsky-thumb.png';
import '../assets/scss/home.scss';

const React = require('react');
// List of items to appear in the home view. May include internal links, external
// links and non-link items. LinkPortfolioItem component handles all of these.

// Description is broken up into an array per span - to add a link, the href should
// be included as the second item in the span array. This allows for links to be included
// in the text area without using dangerouslySetInnerHtml. However, links should only be used
// when the whole item does not have an internal or external link.
const porfolioItems = [
    {
        backgroundColour: '#427AA9',
        foregroundColour: '#fff',
        internalLinkTo: 'skills',
        textHead: 'Skills',
        textDescription: [],
        image: introThumbnail,
    },
    {
        backgroundColour: '#e6e7e8',
        foregroundColour: '#fff',
        internalLinkTo: 'experience-and-qualifications',
        textHead: 'Experience & Qualifications',
        textDescription: [],
        image: identityThumbnail,
    },
    {
        backgroundColour: '#D63F24',
        foregroundColour: '#fff',
        internalLinkTo: 'about',
        textHead: 'About',
        textDescription: [],
        image: illustrationThumbnail,
    },
    {
        backgroundColour: '#e6e7e8',
        foregroundColour: '#fff',
        externalLinkTo: 'https://bitbucket.org/elliel/portfolio-2018/src/master/',
        textHead: 'eten.uk source code ',
        textDescription: [
            ['[React, Webpack, PWA]'],
        ],
    },
    {
        backgroundColour: '#6d6e71',
        foregroundColour: '#0D2256',
        externalLinkTo: 'https://www.movehub.com/uk',
        textHead: 'Movehub.com',
        textDescription: [],
    },
    {
        backgroundColour: '#e6e7e8',
        foregroundColour: '#fff',
        externalLinkTo: 'http://www.philharmonia.co.uk/stravinsky',
        textHead: 'Stravinsky micro-site',
        textDescription: [],
        image: stravinskyThumbnail,
    },
    {
        backgroundColour: '#ffffff',
        textHead: 'Work Examples: ',
        textDescription: [
            ['Movehub is a recent site for which I did all of the front-end development [Gulp/Babel/Browserify, SCSS, Cypress]. The Stravinsky microsite is a little older, but is included because I was responsible for both design (working from an existing style guide) and full build. This site also won bronze in the '],
            ['2017 Lovie web awards', 'https://winners.lovieawards.eu/#!y=2017&wc=110'],
            [' (Category: Websites - Music).'],
        ],
    },
];

class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            mounted: false,
        };
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({ mounted: true });
        }, 0);
    }

    render() {
        return (
            <ul className={`item-list ${this.state.mounted ? 'fade-in' : ''}`}>
                {porfolioItems.map((item, index) => (
                    <LinkPortfolioItem
                        backgroundColour={item.backgroundColour}
                        foregroundColour={item.foregroundColour}
                        internalLinkTo={item.internalLinkTo}
                        externalLinkTo={item.externalLinkTo}
                        textHead={item.textHead}
                        textDescription={item.textDescription}
                        image={item.image}
                        itemIndex={index}
                        key={`portf-item-${index}`}
                    />
                ))}
            </ul>
        );
    }
}

export default Home;
