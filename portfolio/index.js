import runtime from 'serviceworker-webpack-plugin/lib/runtime';
import { BrowserRouter, Route } from 'react-router-dom';
import { AppProvider } from './components/AppContext';

import Header from './components/Header';
import ExitButton from './components/ExitButton';
import PortfolioPage from './components/PortfolioPage';
import Home from './components/Home';
import './assets/scss/indexCommon.scss';

const React = require('react'),
    ReactDOM = require('react-dom');

const extraJS = {
    serviceWorkerSetup() {
        // Set up service worker (after main page load)
        window.addEventListener('load', () => {
            if ('serviceWorker' in navigator) {
                runtime.register();
            }
        });
    },
};

extraJS.serviceWorkerSetup();

// Here routes are only divided by 'Home' or 'PortfolioPage' - more specificity
// in included in the PortfolioPage component
function Portfolio() {

    return (
        <AppProvider>
            <BrowserRouter>
                <div>
                    <div className="container">
                        <Header name="E R Litten" year="2019" />
                        <Route
                            path="/(skills|experience-and-qualifications|about)"
                            children={({ match }) => (
                                <ExitButton notHomePage={!!match} />
                            )}
                        />
                    </div>

                    <div className="outer-container">
                        <div className="container">
                            <Route exact path="/" component={Home} />
                            <Route
                                path="/(skills|experience-and-qualifications|about)"
                                component={PortfolioPage}
                            />
                        </div>
                    </div>
                </div>
            </BrowserRouter>
        </AppProvider>
    );
}

ReactDOM.render(
    <Portfolio />,
    document.getElementById('portfolio'),
);
