// Complications with <picture> & srcset - not an issue here but reminder note!
const dateString = new Date().toISOString(),
    CACHE_NAME = `ERL-portfolio-cache-${dateString}`;

const { assets } = global.serviceWorkerOption;

const assetsToCache = assets.map((path) => {
    return new URL(path, global.location).toString();
});

// Install - first event a service worker gets - only happens once
// (an update / failure here means a new service worker)
self.addEventListener('install', (event) => {
    // Perform install steps
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then((cache) => {
                return cache.addAll(assetsToCache);
            }),
    );
});

// Install will happen immediately when a service worker is updated
// Activate will happen only once switchover is complete, so this is
// when old caches should be cleared
self.addEventListener('activate', (event) => {
    event.waitUntil(
        // Iterate through caches
        global.caches.keys().then((cacheNames) => {
            return Promise.all(
                cacheNames.map((cacheName) => {
                    // Delete caches that do not match the name of the current one
                    if (cacheName.indexOf(CACHE_NAME) === 0) {
                        return null;
                    }
                    return global.caches.delete(cacheName);
                }),
            );
        }),
    );
});

self.addEventListener('fetch', (event) => {
    event.respondWith(
        // check for results from any caches created by service worker
        caches.match(event.request)
            .then((response) => {
                // Cache hit - return cached value if available
                // (except if e.request.mode is navigate - in this case return the cached
                // version, but also update the version in cache (stale while revalidate))
                if (response) {
                    // avoid error where only if cached is only allowed with request
                    // mode same-origin
                    if (!event.request.mode === 'navigate' || event.request.cache === 'only-if-cached') {
                        return response;
                    }
                }

                // if not, clone request
                const fetchRequest = event.request.clone();

                return fetch(fetchRequest).then(
                    (uncachedResponse) => {
                        // Check uncachedResponse is valid, status is 200, & response type basic
                        // (this indicates it's a request from our origin - avoid caching
                        // 3rd party assets)
                        if (!uncachedResponse || uncachedResponse.status !== 200 || uncachedResponse.type !== 'basic') {
                            return uncachedResponse;
                        }

                        // Clone response so we have one to be used by the browser, one by the cache
                        const responseToCache = uncachedResponse.clone();

                        // Cache clone of uncachedResponse for next time
                        caches.open(CACHE_NAME)
                            .then((cache) => {
                                cache.put(event.request, responseToCache);
                            });

                        // And pass reponse (if it exists and e.request.mode is navigate) or
                        // uncachedResponse to browser
                        return response || uncachedResponse;
                    },
                );
            }),
    );
});
