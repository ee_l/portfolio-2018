const path = require('path');

const HTMLWebpackPlugin = require('html-webpack-plugin'),
    babelPresetReact = require('babel-preset-react'),
    babelPresetEnv = require('@babel/preset-env'),
    UglifyJsPlugin = require('uglifyjs-webpack-plugin'),
    OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin'),
    MiniCssExtractPlugin = require('mini-css-extract-plugin'),
    Autoprefixer = require('autoprefixer'),
    ServiceWorkerWebpackPlugin = require('serviceworker-webpack-plugin'),
    CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = (env, options) => {
    return {
        entry: `${__dirname}/portfolio/index.js`,
        output: {
            filename: 'portf-bundle.js',
            path: path.resolve(__dirname, 'dist'),
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: [babelPresetReact, babelPresetEnv],
                        },
                    },
                },
                {
                    test: /\.scss$/,
                    use: [
                        // Could implement HMR as dev is using style-loader...
                        options.mode !== 'production' ? 'style-loader' : MiniCssExtractPlugin.loader,
                        { // minimise CSS
                            loader: 'css-loader',
                            options: {
                                sourceMap: true,
                                importLoaders: 2,
                            },
                        },
                        { // autoprefix CSS
                            loader: 'postcss-loader',
                            options: {
                                sourceMap: true,
                                ident: 'postcss',
                                plugins: () => [
                                    Autoprefixer(),
                                ],
                            },
                        },
                        { // SCSS to CSS
                            loader: 'sass-loader',
                            options: { sourceMap: true },
                        },
                    ],
                },
                {
                    test: /\.(png|jpg|gif)$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: '[name].[ext]',
                                outputPath: 'images/',
                            },
                        },
                    ],
                },
            ],
        },
        optimization: {
            minimizer: [
                new UglifyJsPlugin({
                    cache: true,
                    parallel: true, // (better speed)
                    sourceMap: true,
                    test: /\.(js)$/,
                    exclude: /node_modules/,
                }),
                new OptimizeCSSAssetsPlugin({}),
            ],
        },
        plugins: [
            new HTMLWebpackPlugin({
                template: `${__dirname}/portfolio/index.html`,
                inject: 'body',
            }),
            new MiniCssExtractPlugin({
                filename: 'styles.css',
            }),
            new ServiceWorkerWebpackPlugin({
                entry: `${__dirname}/portfolio/sw.js`,
            }),
            new CopyWebpackPlugin([
                { from: `${__dirname}/portfolio/icons`, to: `${__dirname}/dist/icons` },
                { from: `${__dirname}/portfolio/sw.js`, to: `${__dirname}/dist/sw.js` },
                { from: `${__dirname}/portfolio/manifest.json`, to: `${__dirname}/dist/manifest.json` },
            ]),
        ],
    };
};
