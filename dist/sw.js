var serviceWorkerOption = {
  "assets": [
    "/images/intro-thumb.png",
    "/images/illustration-thumb.jpg",
    "/images/identity-thumb.jpg",
    "/images/stravinsky-thumb.png",
    "/images/some_turned_into_birds.jpg",
    "/images/shrimp_as_big_as_whales.jpg",
    "/images/albion-1.png",
    "/styles.css",
    "/portf-bundle.js",
    "/1.styles.css",
    "/1.portf-bundle.js",
    "/2.styles.css",
    "/2.portf-bundle.js",
    "/3.styles.css",
    "/3.portf-bundle.js",
    "/index.html"
  ]
};
        
        !function(n){var r={};function o(e){if(r[e])return r[e].exports;var t=r[e]={i:e,l:!1,exports:{}};return n[e].call(t.exports,t,t.exports,o),t.l=!0,t.exports}o.m=n,o.c=r,o.d=function(e,t,n){o.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:n})},o.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},o.t=function(t,e){if(1&e&&(t=o(t)),8&e)return t;if(4&e&&"object"==typeof t&&t&&t.__esModule)return t;var n=Object.create(null);if(o.r(n),Object.defineProperty(n,"default",{enumerable:!0,value:t}),2&e&&"string"!=typeof t)for(var r in t)o.d(n,r,function(e){return t[e]}.bind(null,r));return n},o.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return o.d(t,"a",t),t},o.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},o.p="",o(o.s=0)}([function(e,t,n){(function(t){var e=(new Date).toISOString(),o="ERL-portfolio-cache-".concat(e),n=t.serviceWorkerOption.assets.map(function(e){return new URL(e,t.location).toString()});self.addEventListener("install",function(e){e.waitUntil(caches.open(o).then(function(e){return e.addAll(n)}))}),self.addEventListener("activate",function(e){e.waitUntil(t.caches.keys().then(function(e){return Promise.all(e.map(function(e){return 0===e.indexOf(o)?null:t.caches.delete(e)}))}))}),self.addEventListener("fetch",function(r){r.respondWith(caches.match(r.request).then(function(n){if(n&&("navigate"===!r.request.mode||"only-if-cached"===r.request.cache))return n;var e=r.request.clone();return fetch(e).then(function(e){if(!e||200!==e.status||"basic"!==e.type)return e;var t=e.clone();return caches.open(o).then(function(e){e.put(r.request,t)}),n||e})}))})}).call(this,n(1))},function(e,t){var n;n=function(){return this}();try{n=n||new Function("return this")()}catch(e){"object"==typeof window&&(n=window)}e.exports=n}]);